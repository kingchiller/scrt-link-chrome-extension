import React from 'react';

import './Popup.css';

const Popup = () => {
  return (
    <div className="App">
      <iframe className="App-iframe" src="https://scrt.link/widget" />
    </div>
  );
};

export default Popup;
