<img src="src/assets/img/icon-128.png" width="64"/>

# Chrome Extension for scrt.link

## Installing and Running

- Run `npm install` to install the dependencies.
- Run `npm start`

- Load your extension on Chrome following:
  - Access `chrome://extensions/`
  - Check `Developer mode`
  - Click on `Load unpacked extension`
  - Select the `build` folder.

## Credits

Based on https://github.com/lxieyang/chrome-extension-boilerplate-react by Michael Xieyang Liu | [Website](https://lxieyang.github.io)
